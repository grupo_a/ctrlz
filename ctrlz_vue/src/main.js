import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import Gestor_Vendas from './components/Gestor_Vendas.vue'
import DashBoard from './components/DashBoard.vue'
import Gestor_Compras from './components/Gestor_Compras.vue'
import Ceo from './components/Ceo.vue'


// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.use(VueRouter)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

const routes=[
  {path:'/dashboard', component: DashBoard},
  {path:'/gestorvendas', component: Gestor_Vendas},
  {path:'/gestorCompras', component: Gestor_Compras},
  {path:'/ceo', component: Ceo}
];

const router= new VueRouter ({
  routes,
  mode: 'history'
});

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
