﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebAPI_CtrlZ.Models
{
    public class InvoiceLine
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; } //auto Genetated
        public string InvoiceNo { get; set; }
        public string InvoiceDate { get; set; }
        public string LineNumber { get; set; }
        public string ProductCode { get; set; }
        public string ProductDescription { get; set; }
        public string Quantity { get; set; }
        public string UnitOfMeasure { get; set; }
        public string UnitPrice { get; set; }
        public string TaxPointDate { get; set; }
        public string Description { get; set; }
        public string CreditAmount { get; set; }
    }
}
