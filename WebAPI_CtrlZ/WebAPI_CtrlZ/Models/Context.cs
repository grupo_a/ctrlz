﻿using Microsoft.EntityFrameworkCore;

namespace WebAPI_CtrlZ.Models
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options)
            : base(options)
        {
            //
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<Costumer> Costumer { get; set; }
        public DbSet<Supplier> Supplier { get; set; }
        public DbSet<Transaction> Transaction { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<Invoice> Invoice { get; set; }
        public DbSet<InvoiceLine> InvoiceLine { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Account>().ToTable("Account");
            builder.Entity<Costumer>().ToTable("Costumer");
            builder.Entity<Supplier>().ToTable("Supplier");
            builder.Entity<Transaction>().ToTable("Transaction");
            builder.Entity<Product>().ToTable("Product");
            builder.Entity<Invoice>().ToTable("Invoice");
            builder.Entity<InvoiceLine>().ToTable("InvoiceLine");

            builder.Entity<Account>().HasKey(a => a.Id);
            builder.Entity<Costumer>().HasKey(c => c.Id);
            builder.Entity<Supplier>().HasKey(s => s.Id);
            builder.Entity<Transaction>().HasKey(t => t.Id);
            builder.Entity<Product>().HasKey(t => t.Id);
            builder.Entity<Invoice>().HasKey(t => t.Id);
            builder.Entity<InvoiceLine>().HasKey(t => t.Id);

            builder.Entity<InvoiceLine>().Property(c => c.Id).ValueGeneratedOnAdd();
        }
    }
}