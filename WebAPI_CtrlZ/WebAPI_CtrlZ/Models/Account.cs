﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebAPI_CtrlZ.Models
{
    public class Account
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; } //AccountID
        public string AccountDescription { get; set; }
        public string OpeningDebitBalance { get; set; }
        public string OpeningCreditBalance { get; set; }
        public string ClosingDebitBalance { get; set; }
        public string ClosingCreditBalance { get; set; }
        public string GroupingCategory { get; set; }
        public string GroupingCode { get; set; }

        public override string ToString()
        {
            return Id + " --- " + AccountDescription;
        }
    }
}
