﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebAPI_CtrlZ.Models
{
    public class Invoice
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; } //InvoiceNo
        public string Period { get; set; }
        public string InvoiceDate { get; set; }
        public string InvoiceType { get; set; }
        public string SourceID { get; set; }
        public string SystemEntryDate { get; set; }
        public string TransactionID { get; set; }
        public string CustomerID { get; set; }
        public string MovementStartTime { get; set; }
        public string NetTotal { get; set; }
    }
}
