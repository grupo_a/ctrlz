﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebAPI_CtrlZ.Models
{
    public class Transaction
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; } //TransactionID
        public string Period { get; set; }
        public string TransactionDate { get; set; }
        public string SourceID { get; set; }
        public string Description { get; set; }
        public string DocArchivalNumber { get; set; }
        public string TransactionType { get; set; }
        public DateTime GLPostingDate { get; set; }
        public string CustomerID { get; set; }
        public string SupplierID { get; set; }

        //Lines -> CreditLine
        public string AccountID { get; set; }
        public DateTime SystemEntryDate { get; set; }
        public string Amount { get; set; } //CreditAmount or DebitAmount
    }
}
