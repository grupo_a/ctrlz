﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebAPI_CtrlZ.Models
{
    public class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; } //ProductCode
        public string ProductType { get; set; }
        public string ProductGroup { get; set; }
        public string ProductDescription { get; set; }
        public string ProductNumberCode { get; set; }
    }
}
