﻿using System;
using System.IO;
using WebAPI_CtrlZ.Controllers;

namespace WebAPI_CtrlZ.Models
{
    public class DBInitializer
    {
        public static Context context;

        public static void Initialize(Context context)
        {
            DBInitializer.context = context;
            context.Database.EnsureCreated();

            DirectoryInfo directory = new DirectoryInfo(Directory.GetCurrentDirectory() + "\\SAF-T");

            if (directory.Exists)
            {
                if (directory.GetFiles().Length == 1)
                {
                    if (directory.GetFiles()[0].Extension.ToLower().Contains(".xml"))
                    {
                        if (File.Exists(directory.GetFiles()[0].FullName))
                        {
                            Console.WriteLine("Ficheiro saf-t encontrado.");

                            ExtrasController fileController = new ExtrasController(context);
                            fileController.SetFile(directory.GetFiles()[0].FullName);
                        }
                        else
                        {
                            Console.WriteLine("Erro - Ficheiro não encontrado");
                            return;
                        }
                    }
                    else
                        Console.WriteLine("Ficheiro não é do tipo xml.");
                }
                else
                {
                    Console.WriteLine("Por favor coloque apenas um ficheiro saf-t na pasta.");
                }
            }
            context.SaveChanges();
        }

    }
}
