﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI_CtrlZ.Models;

namespace WebAPI_CtrlZ.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CostumersController : ControllerBase
    {
        private readonly Context _context;

        public CostumersController(Context context)
        {
            _context = context;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetCostumers")]
        public async Task<ActionResult<IEnumerable<Costumer>>> GetCostumers()
        {
            return await _context.Costumer.ToListAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetCostumer")]
        public async Task<ActionResult<Costumer>> GetCostumer(string id)
        {
            var costumer = await _context.Costumer.FindAsync(id);

            if (costumer == null)
            {
                return NotFound();
            }

            return costumer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="costumer"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("PutCostumer")]
        public async Task<IActionResult> PutCostumer(string id, Costumer costumer)
        {
            if (id != costumer.Id)
            {
                return BadRequest();
            }

            _context.Entry(costumer).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CostumerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="costumer"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("PostCostumer")]
        public async Task<ActionResult<Costumer>> PostCostumer(Costumer costumer)
        {
            _context.Costumer.Add(costumer);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCostumer", new { id = costumer.Id }, costumer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("DeleteCostumer")]
        public async Task<ActionResult<Costumer>> DeleteCostumer(string id)
        {
            var costumer = await _context.Costumer.FindAsync(id);
            if (costumer == null)
            {
                return NotFound();
            }

            _context.Costumer.Remove(costumer);
            await _context.SaveChangesAsync();

            return costumer;
        }

        private bool CostumerExists(string id)
        {
            return _context.Costumer.Any(e => e.Id == id);
        }
    }
}
